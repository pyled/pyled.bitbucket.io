import time, machine, neopixel

rows = 8
columns = 8
pixels = rows * columns
data_pin = 4
np = neopixel.NeoPixel(machine.Pin(data_pin), pixels)

green = G = (0, 5, 0)
yellow = Y = (5, 5, 0)
blue = B = (0, 0, 5)
red = R = (5, 0, 0)
white = W = (5,5,5)
nothing = N = (0,0,0)
pink = P = (5,1,3)

d = {}
d[0] = [N, G, G, G, G, G, G, N, G, G, G, G, G, G, G, G, G, G, G, N, N, G, G, G, G, G, G, N, N, G, G, G, G, G, G, N, N, G, G, G, G, G, G, N, N, G, G, G, G, G, G, G, G, G, G, G, N, G, G, G, G, G, G, N]
d[1] = [N, N, N, G, G, G, N, N, N, N, G, G, G, G, N, N, N, G, G, G, G, G, N, N, N, N, N, G, G, G, N, N, N, N, N, G, G, G, N, N, N, N, N, G, G, G, N, N, N, N, N, G, G, G, N, N, N, N, N, G, G, G, N, N]
d[2] = [N, G, G, G, G, G, G, N, G, G, G, G, G, G, G, G, G, G, G, N, N, G, G, G, N, N, N, N, G, G, G, N, N, N, G, G, G, N, N, N, G, G, G, N, N, N, N, N, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G]
d[3] = [B, B, B, B, B, B, B, N, B, B, B, B, B, B, B, B, N, N, N, N, N, B, B, B, B, B, B, B, B, B, B, N, B, B, B, B, B, B, B, N, N, N, N, N, N, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, N]
d[4] = [B, B, B, N, B, B, B, N, B, B, B, N, B, B, B, N, B, B, B, N, B, B, B, N, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, N, N, N, N, B, B, B, N, N, N, N, N, B, B, B, N, N, N, N, N, B, B, B, N]
d[5] = [G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, N, N, N, N, N, G, G, G, G, G, G, G, N, G, G, G, G, G, G, G, G, N, N, N, N, N, G, G, G, G, G, G, G, G, G, G, G, N, G, G, G, G, G, G, N]
d[6] = [N, G, G, G, G, G, G, N, G, G, G, G, G, G, G, G, G, G, G, N, N, N, N, N, G, G, G, G, G, G, G, N, G, G, G, G, G, G, G, G, G, G, G, N, N, G, G, G, G, G, G, G, G, G, G, G, N, G, G, G, G, G, G, N]
d[7] = [G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, G, N, N, N, N, N, G, G, G, N, N, N, N, G, G, G, N, N, N, N, G, G, G, N, N, N, N, G, G, G, N, N, N, N, N, G, G, G, N, N, N, N, N, G, G, G, N, N, N]
d[8] = [N, G, G, G, G, G, G, N, G, G, G, G, G, G, G, G, G, G, G, N, N, G, G, G, N, G, G, G, G, G, G, N, N, G, G, G, G, G, G, N, G, G, G, N, N, G, G, G, G, G, G, G, G, G, G, G, N, G, G, G, G, G, G, N]
d[9] = [N, G, G, G, G, G, G, N, G, G, G, G, G, G, G, G, G, G, G, N, N, G, G, G, G, G, G, G, G, G, G, G, N, G, G, G, G, G, G, G, N, N, N, N, N, G, G, G, G, G, G, G, G, G, G, G, N, G, G, G, G, G, G, N]

l = {}
l['x'] = [R, R, N, N, N, N, R, R, R, R, R, N, N, R, R, R, N, R, R, R, R, R, R, N, N, N, R, R, R, R, N, N, N, N, R, R, R, R, N, N, N, R, R, R, R, R, R, N, R, R, R, N, N, R, R, R, R, R, N, N, N, N, R, R]

def red(x = None, y = None):
    color = (5, 0, 0)
    pixel(x, y, color)

def green(x = None, y = None):
    color = (0, 5, 0)
    pixel(x, y, color)

def blue(x = None, y = None):
    color = (0, 0, 5)
    pixel(x, y, color)

def pixel(x, y, color):
    if x is None and y is None:
        return print("Error: x / y")
    
    led_id = get_led_id(x, y)    
    np[led_id] = color    
    np.write()

def column(x, color_name = 'green'):
    for y in range(columns):
        pixel(x, y, name_to_color(color_name))
    
def row(y, color_name = 'green'):
    for x in range(rows):
        pixel(x + 1, y, name_to_color(color_name))
    
def name_to_color(color_name):
    return {
        'red': (5, 0, 0),
        'green': (0, 5, 0),
        'blue': (0, 0, 5),
    }.get(color_name, (5,0,0))
    
def get_led_id(x, y):
    led_id = (y - 1) * columns + x - 1
    return led_id
    
def clear():
    for i in range(pixels):
        np[i] = (0, 0, 0)
    np.write()
    
def bitmap(bmp):
    for i in range(0,63):
        np[i] = bmp[i]
    np.write()

def ip_led(delay = 700):
    import network
    sta_if = network.WLAN(network.STA_IF)
    ip = sta_if.ifconfig()[0]
    last_segment = int(ip.split('.')[3])
    hundreds = tens = ones = 0
    
    if last_segment >= 100:
        hundreds, tens = divmod(last_segment, 100)
        bitmap(d[hundreds])        
        time.sleep_ms(delay)
        clear()
        time.sleep_ms(200)
    if last_segment >= 10:
        tens, ones = divmod(tens, 10)
        bitmap(d[tens])
        time.sleep_ms(delay)
        clear()
        time.sleep_ms(200)
    
    bitmap(d[ones])
    time.sleep_ms(delay)
    clear()

def loader_steps(seconds_to_full = 5):
    x = 4
    y = 4
    steps = []
    steps.append((x, y))
    
    mode = 'right'    
    
    for i in range(1, 9):
        if mode == 'right':
            for j in range(1, i + 1):
                x = x + 1
                steps.append((x, y))
            for k in range(1, i + 1):
                y = y + 1
                steps.append((x, y))
            mode = 'left'            
        elif mode == 'left':
            for j in range(1, i + 1):
                x = x - 1
                if x == 0:
                    return steps
                steps.append((x, y))
            for k in range(1, i + 1):
                y = y - 1
                steps.append((x, y))
            mode = 'right'
        
    return steps
    
def loader_example(delay = 100):
    steps = loader_steps()
    colors = [ (0,5,0), (0,0,5), (5,5,0), (5,0,0) ]
    counter = 0
    step = 0
    while True:    
        iter, step = divmod(counter, len(steps))
        cdiv, color = divmod(iter, len(colors))
        x, y = steps[step]        
        pixel(x, y, colors[color])
        time.sleep_ms(delay)
        counter = counter + 1
